from django.contrib.auth.models import User
from django.test import Client, TestCase
from user.models import Profile
from .models import Project, ProjectCategory, Task, TaskOffer
from .views import get_user_task_permissions



class ProjectViewTest(TestCase):

    def setUp(self):
        self.client = Client()
        owner = User.objects.create_user(username='lea', password='lea123')
        category= ProjectCategory.objects.create(name='Gardering')
        Project.objects.create(user=owner.profile, title='Savage Garden', description='To the monn and back', category=category, status='o')

    def test_status_change(self):
        self.client.login(username="lea", password="lea123")
        project = Project.objects.get(title='Savage Garden')
        path = '/projects/' + str(project.id) + '/'
        response = self.client.post(path, {
            'status_change': 'status_change',
            'status': 'f'
        })
        updated_project = Project.objects.get(title='Savage Garden')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(updated_project.status, 'f')



    def test_offer_response(self):
        self.client.login(username="lea", password="lea123")
        project = Project.objects.get(title='Savage Garden')
        task = Task.objects.create(project=project, title="Mowing", status='pa')
        offerer = User.objects.create_user(username='joe', password='joe123').profile
        TaskOffer.objects.create(task = task, title="taskOffer", offerer=offerer, status='a')
        path = '/projects/' + str(project.id) + '/'
        response = self.client.post(path, {
            'status' : 'a',
            'feedback': 'This is simple feedback.',
            'taskofferid': TaskOffer.objects.get(title='taskOffer').id,
            'offer_response': 'offer_response'
        })
        updated_taskOffer = TaskOffer.objects.get(title='taskOffer')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(updated_taskOffer.feedback, 'This is simple feedback.')


    def test_offer_submit(self):
        User.objects.create_user(username='joe', password='joe123')
        self.client.login(username="joe", password="joe123")
        project = Project.objects.get(title='Savage Garden')
        path = '/projects/' + str(project.id) + '/'
        Task.objects.create(title='Test', project=project)
        response= self.client.post(path, {
            'offer_submit': 'offer_submit',
            'title': 'Simple title',
            'description': 'This is description',
            'price': 100,
            'taskvalue': Task.objects.get(title='Test').id #Id of task
        })
        self.assertEqual(response.status_code, 200)



class UserPermissionsTest(TestCase):
    def setUp(self):
        owner = User.objects.create_user(username='bob', password='bob123')
        project = Project.objects.create(user=owner.profile, title='Flowers', 
        category = ProjectCategory.objects.create(name='Gardering'), status = 'i')
        Task.objects.create(project=project, title='Bloom')

    def test_is_project_owner(self):
        user = User.objects.get(username='bob')
        permission = get_user_task_permissions(user, Task.objects.get(title='Bloom'))
        self.assertEqual(permission,{
            'write': True,
            'read': True,
            'modify': True,
            'owner': True,
            'upload': True,
        })

    def test_is_task_offerer(self):
        user = User.objects.create_user(username='pia')
        task = Task.objects.get(title='Bloom')
        TaskOffer.objects.create(task=task, title='Test Offer', offerer=user.profile, status='a')
        permission = get_user_task_permissions(user, task)
        self.assertEqual(permission, {
            'write': True,
            'read': True,
            'modify': True,
            'owner': False,
            'upload': True,
        })

    def test_rest_permissions(self):
        task = Task.objects.get(title='Bloom')
        user = User.objects.create_user(username='rest')
        self.assertEqual(get_user_task_permissions(user, task), {
            'write': False,
            'read': False,
            'modify': False,
            'owner': False,
            'view_task': False,
            'upload': False,
        })
